#README

##Patient Registry Application

Application to manage patients and their doctor appointments.

##Build

mvn clean install

##Run

mvn spring-boot:run

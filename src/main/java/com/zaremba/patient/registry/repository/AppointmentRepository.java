package com.zaremba.patient.registry.repository;

import com.zaremba.patient.registry.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    @Query("select appointment from Appointment appointment where appointment.patient.token = ?1")
    Stream<Appointment> getAppointmentsByPatientToken(String patientToken);

    Optional<Appointment> getAppointmentByToken(String token);

}

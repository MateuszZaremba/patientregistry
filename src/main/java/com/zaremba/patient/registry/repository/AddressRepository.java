package com.zaremba.patient.registry.repository;

import com.zaremba.patient.registry.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface AddressRepository extends JpaRepository<Address, Long>, QueryByExampleExecutor<Address> {
}

package com.zaremba.patient.registry.repository;

import com.zaremba.patient.registry.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    Optional<Patient> findByToken(String token);
}

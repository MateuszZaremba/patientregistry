package com.zaremba.patient.registry.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode(exclude = {"createDate", "modifyDate"})
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "create_date")
    protected LocalDateTime createDate = LocalDateTime.now();

    @Column(name = "modify_date")
    protected LocalDateTime modifyDate = LocalDateTime.now();

    @PreUpdate
    public void preUpdate() {
        modifyDate = LocalDateTime.now();
    }

    @PrePersist
    public void prePersist() {
        createDate = LocalDateTime.now();
    }
}

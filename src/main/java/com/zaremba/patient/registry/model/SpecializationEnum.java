package com.zaremba.patient.registry.model;


public enum SpecializationEnum {
    ALLERGIST,
    NEUROLOGIST
}

package com.zaremba.patient.registry.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "doctors")
public class Doctor extends BaseEntity {

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String token;

    @Column
    @Enumerated(EnumType.STRING)
    private SpecializationEnum specialization;

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.PERSIST)
    private List<Appointment> appointments;

    @Override
    public String toString() {
        return "Doctor{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", token='" + token + '\'' +
                ", specialization=" + specialization +
                '}';
    }
}

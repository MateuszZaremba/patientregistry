package com.zaremba.patient.registry.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "appointments")
public class Appointment extends BaseEntity {

    @Column
    private LocalDateTime date;

    @OneToOne(cascade = CascadeType.PERSIST)
    private Address address;

    @Column
    private String description;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Patient patient;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Doctor doctor;

    @Column
    private String token;

    @Override
    public String toString() {
        return "Appointment{" +
                "date=" + date +
                ", address=" + address +
                ", description='" + description + '\'' +
                ", patient=" + patient +
                ", doctor=" + doctor +
                ", token='" + token + '\'' +
                '}';
    }
}

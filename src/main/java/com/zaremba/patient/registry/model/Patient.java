package com.zaremba.patient.registry.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "patients")
public class Patient extends BaseEntity {

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String telephone;

    @Column
    private String email;

    @Column
    private Boolean hasInsurance;

    @Column
    private Integer age;

    @Column
    private String token;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.PERSIST)
    private List<Appointment> appointments;

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", hasInsurance=" + hasInsurance +
                ", age=" + age +
                ", token='" + token + '\'' +
                '}';
    }
}

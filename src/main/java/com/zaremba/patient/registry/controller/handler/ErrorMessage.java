package com.zaremba.patient.registry.controller.handler;

import lombok.Getter;

public class ErrorMessage {

    @Getter
    private String message;

    public ErrorMessage(String response) {
        this.message = response;
    }
}


package com.zaremba.patient.registry.controller;

import com.zaremba.patient.registry.dto.AppointmentDto;
import com.zaremba.patient.registry.service.AppointmentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;

    @GetMapping(value = "/patient/{patient-token}")
    public ResponseEntity<List<AppointmentDto>> getPatientAppointments(@PathVariable("patient-token") String patientToken) {
        return ResponseEntity.ok(appointmentService.getPatientAppointments(patientToken));
    }

    @PostMapping(value = "/create")
    public ResponseEntity<AppointmentDto> createPatientAppointment(@RequestBody AppointmentDto appointmentDto) {
        return ResponseEntity.ok(appointmentService.createAppointment(appointmentDto));
    }

    @DeleteMapping(value = "/delete/{appointment-token}")
    public ResponseEntity deletePatientAppointment(@PathVariable("appointment-token") String appointmentToken) {
        return ResponseEntity.ok(appointmentService.deleteAppointment(appointmentToken));
    }

}

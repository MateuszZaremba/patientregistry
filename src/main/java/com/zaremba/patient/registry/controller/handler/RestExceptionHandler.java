package com.zaremba.patient.registry.controller.handler;

import com.zaremba.patient.registry.exception.BaseException;
import com.zaremba.patient.registry.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handleNoSuchObject(BaseException e) {
        return new ErrorMessage(e.getMessage());
    }

}

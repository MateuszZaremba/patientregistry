package com.zaremba.patient.registry.service;

import com.zaremba.patient.registry.dto.AppointmentDto;
import com.zaremba.patient.registry.exception.NotFoundException;
import com.zaremba.patient.registry.model.Address;
import com.zaremba.patient.registry.model.Appointment;
import com.zaremba.patient.registry.model.Doctor;
import com.zaremba.patient.registry.model.Patient;
import com.zaremba.patient.registry.repository.AddressRepository;
import com.zaremba.patient.registry.repository.AppointmentRepository;
import com.zaremba.patient.registry.repository.DoctorRepository;
import com.zaremba.patient.registry.repository.PatientRepository;
import com.zaremba.patient.registry.utils.TokenGenerator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final AddressRepository addressRepository;

    @Override
    @Transactional
    public List<AppointmentDto> getPatientAppointments(String patientToken) {
        try (Stream<Appointment> result = appointmentRepository.getAppointmentsByPatientToken(patientToken)) {
            return result.map(AppointmentDto::new).collect(Collectors.toList());
        }
    }

    @Override
    public AppointmentDto createAppointment(AppointmentDto appointmentDto) {
        Patient patient = patientRepository.findByToken(appointmentDto.getPatient().getToken())
                .orElseThrow(() -> new NotFoundException("Patient with given token does not exist"));
        Doctor doctor = doctorRepository.findByToken(appointmentDto.getDoctor().getToken())
                .orElseThrow(() -> new NotFoundException("Doctor with given token does not exist"));

        ExampleMatcher matcher = ExampleMatcher.matching().withIncludeNullValues().withIgnorePaths("id", "createDate", "modifyDate");

        Example<Address> example = Example.of(
                new Address(appointmentDto.getAddress().getCountry(),
                        appointmentDto.getAddress().getCity(),
                        appointmentDto.getAddress().getPostalCode(),
                        appointmentDto.getAddress().getStreet()), matcher);
        Address address = addressRepository.findOne(example);

        if (address == null) {
            throw new NotFoundException("Given address not found");
        }

        Appointment appointment = new Appointment();
        appointment.setToken(TokenGenerator.generateRandomToken());
        appointment.setDescription(appointmentDto.getDescription());
        appointment.setDoctor(doctor);
        appointment.setPatient(patient);
        appointment.setAddress(address);
        appointment.setDate(appointmentDto.getDate());

        patient.getAppointments().add(appointment);
        doctor.getAppointments().add(appointment);

        return new AppointmentDto(appointmentRepository.save(appointment));
    }

    @Override
    public boolean deleteAppointment(String appointmentToken) {
        Appointment appointment = appointmentRepository.getAppointmentByToken(appointmentToken)
                .orElseThrow(() -> new NotFoundException("Appointment with given token not found."));

        appointmentRepository.delete(appointment.getId());
        return true;
    }
}

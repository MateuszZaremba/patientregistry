package com.zaremba.patient.registry.service;


import com.zaremba.patient.registry.dto.AppointmentDto;

import java.util.List;

public interface AppointmentService {
    List<AppointmentDto> getPatientAppointments(String patientToken);

    AppointmentDto createAppointment(AppointmentDto appointmentDto);

    boolean deleteAppointment(String appointmentToken);
}

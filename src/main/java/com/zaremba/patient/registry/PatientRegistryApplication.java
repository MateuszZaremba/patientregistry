package com.zaremba.patient.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.zaremba.patient.registry")
public class PatientRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(PatientRegistryApplication.class, args);
    }
}

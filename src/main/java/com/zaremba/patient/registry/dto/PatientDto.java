package com.zaremba.patient.registry.dto;

import com.zaremba.patient.registry.model.Patient;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PatientDto {

    private String firstName;

    private String lastName;

    private String telephone;

    private String email;

    private Boolean hasInsurance;

    private Integer age;

    private String token;

    public PatientDto(Patient patient) {
        firstName = patient.getFirstName();
        lastName = patient.getLastName();
        telephone = patient.getTelephone();
        email = patient.getEmail();
        hasInsurance = patient.getHasInsurance();
        age = patient.getAge();
        token = patient.getToken();
    }
}

package com.zaremba.patient.registry.dto;

import com.zaremba.patient.registry.model.Appointment;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class AppointmentDto {

    private LocalDateTime date;

    private AddressDto address;

    private String description;

    private PatientDto patient;

    private DoctorDto doctor;

    private String token;

    public AppointmentDto(Appointment appointment) {
        date = appointment.getDate();
        address = new AddressDto(appointment.getAddress());
        description = appointment.getDescription();
        patient = new PatientDto(appointment.getPatient());
        doctor = new DoctorDto(appointment.getDoctor());
        token = appointment.getToken();
    }
}

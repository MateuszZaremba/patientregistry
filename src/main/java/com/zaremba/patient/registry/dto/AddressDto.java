package com.zaremba.patient.registry.dto;

import com.zaremba.patient.registry.model.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddressDto {

    private String country;

    private String city;

    private String postalCode;

    private String street;

    public AddressDto(Address address) {
        country = address.getCountry();
        city = address.getCity();
        postalCode = address.getPostalCode();
        street = address.getStreet();
    }
}

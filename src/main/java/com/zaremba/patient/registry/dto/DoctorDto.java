package com.zaremba.patient.registry.dto;

import com.zaremba.patient.registry.model.Doctor;
import com.zaremba.patient.registry.model.SpecializationEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DoctorDto {

    private String firstName;

    private String lastName;

    private SpecializationEnum specialization;

    private String token;

    public DoctorDto(Doctor doctor) {
        firstName = doctor.getFirstName();
        lastName = doctor.getLastName();
        specialization = doctor.getSpecialization();
        token = doctor.getToken();
    }
}

package com.zaremba.patient.registry.utils;


import org.apache.tomcat.util.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class TokenGenerator {

    private static Random random = new Random();

    private static String SHA256(String text) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] bytes = messageDigest.digest(text.getBytes());
            return Base64.encodeBase64String(bytes);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String generateRandomToken() {
        return TokenGenerator.SHA256(System.nanoTime() + "" + random.nextInt()).replaceAll("[^a-zA-Z0-9]", "");
    }


}

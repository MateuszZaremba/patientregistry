package com.zaremba.patient.registry.exception;

public class NotFoundException extends BaseException {
    public NotFoundException(String message) {
        super(message);
    }
}

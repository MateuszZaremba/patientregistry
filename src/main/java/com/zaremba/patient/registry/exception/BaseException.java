package com.zaremba.patient.registry.exception;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {

    private String message;

    public BaseException(String message) {
        super(message);
        this.message = message;
    }
}


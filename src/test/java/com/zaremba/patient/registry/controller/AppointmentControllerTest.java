package com.zaremba.patient.registry.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zaremba.patient.registry.PatientRegistryApplicationTests;
import com.zaremba.patient.registry.dto.AddressDto;
import com.zaremba.patient.registry.dto.AppointmentDto;
import com.zaremba.patient.registry.dto.DoctorDto;
import com.zaremba.patient.registry.dto.PatientDto;
import com.zaremba.patient.registry.model.*;
import com.zaremba.patient.registry.repository.AppointmentRepository;
import com.zaremba.patient.registry.utils.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PatientRegistryApplicationTests.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AppointmentControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AppointmentRepository appointmentRepository;

    private MockMvc mockMvc;

    private Address address;

    private Patient patient;

    private Doctor doctor;

    private Appointment appointment;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();

        appointment = new Appointment();
        appointment.setDate(LocalDateTime.now());
        appointment.setDescription("Doctor appointment");
        appointment.setToken("AppointmentToken");

        address = new Address();
        address.setCountry("Poland");
        address.setCity("Warsaw");
        address.setPostalCode("05-200");
        address.setStreet("Aleje Jerozolimskie 96");

        appointment.setAddress(address);

        patient = new Patient();
        patient.setFirstName("John");
        patient.setLastName("Kowalsky");
        patient.setAge(33);
        patient.setEmail("j.kowalsky@wp.pl");
        patient.setHasInsurance(true);
        patient.setTelephone("111 111 111");
        patient.setToken("testToken");
        patient.setAppointments(Stream.of(appointment).collect(Collectors.toList()));

        appointment.setPatient(patient);

        doctor = new Doctor();
        doctor.setFirstName("Peter");
        doctor.setLastName("Bob");
        doctor.setToken("doctorToken");
        doctor.setSpecialization(SpecializationEnum.ALLERGIST);
        doctor.setAppointments(Stream.of(appointment).collect(Collectors.toList()));

        appointment.setDoctor(doctor);

        appointmentRepository.save(appointment);
    }

    @Test
    public void when_getPatientAppointmentsWithValidToken_thenReturnOK() throws Exception {
        mockMvc.perform(
                get("/appointments/patient/testToken")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].description", Matchers.is(appointment.getDescription())))
                .andExpect(jsonPath("$.[0].date", Matchers.is(appointment.getDate().toString())))
                .andExpect(jsonPath("$.[0].patient.age", Matchers.is(patient.getAge())))
                .andExpect(jsonPath("$.[0].patient.firstName", Matchers.is(patient.getFirstName())))
                .andExpect(jsonPath("$.[0].patient.lastName", Matchers.is(patient.getLastName())))
                .andExpect(jsonPath("$.[0].patient.email", Matchers.is(patient.getEmail())))
                .andExpect(jsonPath("$.[0].patient.hasInsurance", Matchers.is(patient.getHasInsurance())))
                .andExpect(jsonPath("$.[0].patient.telephone", Matchers.is(patient.getTelephone())))
                .andExpect(jsonPath("$.[0].patient.token", Matchers.is(patient.getToken())))
                .andExpect(jsonPath("$.[0].doctor.lastName", Matchers.is(doctor.getLastName())))
                .andExpect(jsonPath("$.[0].doctor.firstName", Matchers.is(doctor.getFirstName())))
                .andExpect(jsonPath("$.[0].doctor.specialization", Matchers.is(doctor.getSpecialization().toString())))
                .andExpect(jsonPath("$.[0].doctor.token", Matchers.is(doctor.getToken())));
    }

    @Test
    public void when_getPatientAppointmentsWithInvalidToken_thenReturnEmptyList() throws Exception {
        mockMvc.perform(
                get("/appointments/patient/invalidToken")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"));
    }

    @Test
    @Transactional
    public void when_createtPatientAppointmentWithValidData_thenReturnOK() throws Exception {
        AppointmentDto appointmentDto = createAppointmentDto();

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        mockMvc.perform(
                post("/appointments/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(appointmentDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description", Matchers.is(appointmentDto.getDescription())))
                .andExpect(jsonPath("$.patient.age", Matchers.is(patient.getAge())))
                .andExpect(jsonPath("$.patient.firstName", Matchers.is(patient.getFirstName())))
                .andExpect(jsonPath("$.patient.lastName", Matchers.is(patient.getLastName())))
                .andExpect(jsonPath("$.patient.email", Matchers.is(patient.getEmail())))
                .andExpect(jsonPath("$.patient.hasInsurance", Matchers.is(patient.getHasInsurance())))
                .andExpect(jsonPath("$.patient.telephone", Matchers.is(patient.getTelephone())))
                .andExpect(jsonPath("$.patient.token", Matchers.is(patient.getToken())))
                .andExpect(jsonPath("$.doctor.lastName", Matchers.is(doctor.getLastName())))
                .andExpect(jsonPath("$.doctor.firstName", Matchers.is(doctor.getFirstName())))
                .andExpect(jsonPath("$.doctor.specialization", Matchers.is(doctor.getSpecialization().toString())))
                .andExpect(jsonPath("$.doctor.token", Matchers.is(doctor.getToken())));

        assertEquals(2, appointmentRepository.getAppointmentsByPatientToken(patient.getToken()).collect(Collectors.toList()).size());
    }

    @Test
    public void when_createtPatientAppointmentWithInvalidPatientData_thenReturnError() throws Exception {
        AppointmentDto appointmentDto = createAppointmentDto();
        appointmentDto.getPatient().setToken("InvalidPatientToken");

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        mockMvc.perform(
                post("/appointments/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(appointmentDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    @Transactional
    public void when_createtPatientAppointmentWithInvalidDoctorData_thenReturnError() throws Exception {
        AppointmentDto appointmentDto = createAppointmentDto();
        appointmentDto.getDoctor().setToken("Invalid");

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        mockMvc.perform(
                post("/appointments/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(appointmentDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void when_deletePatientAppointmentWithValidData_thenReturnOK() throws Exception {

        mockMvc.perform(
                delete("/appointments/delete/{appointmentToken}", appointment.getToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        assertEquals(Optional.empty(), appointmentRepository.getAppointmentByToken(appointment.getToken()));
    }

    @Test
    public void when_deletePatientAppointmentWithInvalidData_thenReturnOK() throws Exception {

        mockMvc.perform(
                delete("/appointments/delete/{appointmentToken}", "invalidAppointmentToken")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    private AppointmentDto createAppointmentDto() {
        LocalDateTime appointmentDate = LocalDateTime.now();

        AppointmentDto appointmentDto = new AppointmentDto();
        appointmentDto.setDate(appointmentDate);
        appointmentDto.setDescription("Second appointment");

        PatientDto patientDto = new PatientDto(patient);
        DoctorDto doctorDto = new DoctorDto(doctor);
        AddressDto addressDto = new AddressDto(address);

        appointmentDto.setAddress(addressDto);
        appointmentDto.setDoctor(doctorDto);
        appointmentDto.setPatient(patientDto);
        return appointmentDto;
    }


}
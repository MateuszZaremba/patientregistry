package com.zaremba.patient.registry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ComponentScan("com.zaremba.patient.registry")
@SpringBootTest
public class PatientRegistryApplicationTests {

    @Test
    public void contextLoads() {
    }
}
